package com.adobe.aem.guides.wknd.core.sample.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.aem.guides.wknd.core.sample.services.SampleService;

@Model(adaptables = Resource.class)
public class Banner {

	final static Logger logger = LoggerFactory.getLogger(Banner.class);

	@Inject
	private SampleService sampleService;

	private String message;

	@PostConstruct
	public void init() {
		logger.info("##############################################################calling the init method");
		message = sampleService.getName();
	}

	public String getMessage() {
		logger.info("##############################################################inside the get message method");
		return message;
	}

}