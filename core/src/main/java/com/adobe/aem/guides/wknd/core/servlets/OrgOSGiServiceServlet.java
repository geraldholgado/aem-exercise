package com.adobe.aem.guides.wknd.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;

@Component(service = Servlet.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=TitleSlindServlet Demo Servlet",
		"sling.servlet.paths=/bin/sample/titleservlet", "sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.extensions=" + "html"

})

public class OrgOSGiServiceServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		response.setHeader("Content-Type", "text/html");

		response.getWriter().print("<h1>Sling Servlet injected this title");

		response.getWriter().close();

	}

}