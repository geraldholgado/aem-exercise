package com.adobe.aem.guides.wknd.core.components.impl;

import java.text.ParseException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.adobe.aem.guides.wknd.core.DateUtils;
import com.adobe.aem.guides.wknd.core.components.EventAdvertisement;

@Model(adaptables = { SlingHttpServletRequest.class }, adapters = { EventAdvertisement.class }, resourceType = {
		EventAdvertisementImpl.RESOURCE_TYPE }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class EventAdvertisementImpl implements EventAdvertisement {

	protected static final String RESOURCE_TYPE = "wknd/components/content/eventadvertisement";

	@ValueMapValue
	private String eventImage;

	@ValueMapValue
	private String eventTitle;

	@ValueMapValue
	private String startDate;

	@ValueMapValue
	private String endDate;

	@ValueMapValue
	private String shortDescription;

	@ValueMapValue
	private String[] locations;

	@ValueMapValue
	private String category;

	@ValueMapValue
	private String feeType;

	@Override
	public boolean isEmpty() {
		if (null == eventImage || eventImage.isEmpty())
			return true;
		return false;
	}

	@Override
	public String getEventImage() {
		return eventImage;
	}

	@Override
	public String getEventTitle() {
		return eventTitle;
	}

	@Override
	public String getDisplayDate() {

		try {
			String startDate = DateUtils.formatDate(this.startDate);
			String endDate = DateUtils.formatDate(this.endDate);
			if (startDate.equalsIgnoreCase(endDate))
				return startDate;

			return startDate + " - " + endDate;
		} catch (ParseException e) {
			System.out.println("Error : getDisplayDate : " + e);
			return "";
		}

	}

	@Override
	public String getShortDescription() {
		return shortDescription;
	}

	@Override
	public String getLocation() {
		String locationList = "";
		if (locations.length > 0)
			for (int i = 0; i < locations.length; i++)
				locationList += i == 0 ? locations[i] : ", " + locations[i];

		return locationList;
	}

	@Override
	public String getFirstLocation() {
		if (locations.length > 0)
			return locations[0];
		return "";
	}

	@Override
	public String getCategory() {
		return category;
	}

	@Override
	public String getFeeType() {
		return feeType;
	}

	@Override
	public String getStartDate() {
		return startDate;
	}

	@Override
	public String getEndDate() {
		return endDate;
	}
}
