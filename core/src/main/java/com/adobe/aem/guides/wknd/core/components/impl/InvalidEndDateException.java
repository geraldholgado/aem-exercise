package com.adobe.aem.guides.wknd.core.components.impl;

@SuppressWarnings("serial")
public class InvalidEndDateException extends Exception {

	public InvalidEndDateException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidEndDateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidEndDateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidEndDateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidEndDateException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
