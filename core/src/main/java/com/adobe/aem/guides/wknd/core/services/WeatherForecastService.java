package com.adobe.aem.guides.wknd.core.services;

import java.io.IOException;
import java.text.ParseException;

import com.adobe.aem.guides.wknd.core.exceptions.InvalidAppKeyException;
import com.adobe.aem.guides.wknd.core.exceptions.InvalidParamsException;
import com.adobe.aem.guides.wknd.core.models.WeatherForecast;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface WeatherForecastService {

	public WeatherForecast getWeatherForecast(String city, String date)
			throws IOException, JsonProcessingException, ParseException, InvalidParamsException, InvalidAppKeyException;
}
