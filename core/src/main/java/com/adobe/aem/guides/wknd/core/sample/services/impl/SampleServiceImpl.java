package com.adobe.aem.guides.wknd.core.sample.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.aem.guides.wknd.core.sample.interfaces.Configuration;
import com.adobe.aem.guides.wknd.core.sample.services.SampleService;

@Component(service = SampleService.class, immediate = true, configurationPid = "com.adobe.aem.guides.wknd.core.sample.services.impl.SampleServiceImpl", configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = Configuration.class)
public class SampleServiceImpl implements SampleService {

	final static Logger logger = LoggerFactory.getLogger(SampleServiceImpl.class);

	private Configuration config;

	private String name;

	@Activate
	@Modified
	protected final void activate(Configuration configuration) {
		logger.info(
				"##############################################################calling activate method inside the sample service impl class");
		this.config = configuration;
		name = config.getText();
	}

	@Deactivate
	protected void deactivate() {
	}

	@Override
	public String getName() {
		logger.info(
				"##############################################################calling get name method inside the sample service impl class");
		return name;
	}
}
