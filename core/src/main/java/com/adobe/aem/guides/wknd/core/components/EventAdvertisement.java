package com.adobe.aem.guides.wknd.core.components;

public interface EventAdvertisement {

	String getEventImage();
	
	String getEventTitle();
	
	String getShortDescription();

	String getLocation();
	
	String getFirstLocation();

	String getCategory();
	
	String getFeeType();
	
	String getDisplayDate();
	
	String getStartDate();
	
	String getEndDate();
	
	boolean isEmpty();
}
