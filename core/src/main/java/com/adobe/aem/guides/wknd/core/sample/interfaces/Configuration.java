package com.adobe.aem.guides.wknd.core.sample.interfaces;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "AEMSampleService Configuration", description = "Configuration file")
public @interface Configuration {

	@AttributeDefinition(name = "String Property", description = "Sample String property", type = AttributeType.STRING)
	public String getText() default "It's a test";
}